class User < ActiveRecord::Base
  validates_presence_of :name
  validates_presence_of :email
  validates :age, numericality: { allow_nil: false, only_integer: true, greater_than_or_equal_to: 18 }

  has_many :bets

  scope :fully_adults, ->{ where('age >= 21') }

 def fully_adult?
   age >= 21
 end

  # validate :check_is_adult

  # private
  #
  # def check_is_adult
  #   errors.add(:age, 'Usuario debe ser mayor de 18 anhos') unless age.to_i >= 18
  # end
end