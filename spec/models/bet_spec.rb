require 'rails_helper'

RSpec.describe Bet, type: :model do
  let(:matias) { User.create!(name: "Matias", email: "matias@gmail.com", age: 22)}

  describe '#valid?' do
    subject { Bet.create!(amount: 199, description: "poker", user: matias) }

    it '' do
      is_expected.to validate_presence_of(:amount)
      is_expected.to validate_presence_of(:description)
      is_expected.to validate_presence_of(:user)
    end
  end

  describe 'user' do
    let(:poker_bet) { Bet.create!(amount: 199, description: "poker", user: matias) }

    it 'a bet knows its user' do
      expect(poker_bet.user).to eq(matias)
    end
  end
end
