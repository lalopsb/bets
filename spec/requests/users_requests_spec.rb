require 'rails_helper'

RSpec.describe UsersController, type: :request do
  describe "#index" do

    let(:json_response) { JSON.load(response.body) }
    let(:params) { {} }

    subject { get("/users", params) }

    context "when there are no users" do
      it "returns an empty list" do
        subject

        expect(json_response['users'].length).to eq(0)
      end
    end

    context 'when there are users' do
      let!(:adult_marcus) { FactoryBot.create(:user, :adult, name: "Marcus") }
      let!(:teen_jules) { FactoryBot.create(:user, :teen, name: "Jules") }

      it 'returns a list of users' do
        subject

        expect(json_response['users'].length).to eq(2)
      end

      it 'returns name and id of each' do
        subject

        expect(json_response['users']).to contain_exactly(user_response(adult_marcus), user_response(teen_jules) )
      end
    end

    context 'when we look for just the fully adult users' do
      context 'and there are some adults' do
        let(:params) { { fully_adults: true } }
        let!(:adult) { FactoryBot.create(:user, :adult) }
        let!(:teen) { FactoryBot.create(:user, :teen) }

        it 'returns a list with the adults' do
          subject

          expect(json_response['users']).to contain_exactly(user_response(adult))
        end
      end
    end
  end

  describe '#create' do

    subject { post('/users', params) }

    context 'when the request has the right attributes' do
      let(:params) do
        { name: "Walter", email: "w@gmail.com", age: 32 }
      end

      it "returns 'ok' http status" do
        subject

        expect(response).to have_http_status(:created)
      end

      it 'creates the user' do
        subject

        expect(User.exists?(params)).to be_truthy
      end
    end

    context 'when it receives invalid parameters' do
      let(:params) do
        { id: 4, name: "Walter", email: "w@gmail.com", age: 32 }
      end

      it 'doesnt create the user' do
        subject

        expect(User.exists?(params)).to be_falsey
      end
    end
  end

  def user_response(user)
    {
        'id' => user.id,
        'name' => user.name,
    }
  end
end
