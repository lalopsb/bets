require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'validations' do
    subject { User.create(name: "Matias", email: "matias@gmail.com") }

    it '' do
      is_expected.to validate_presence_of(:name)
      is_expected.to validate_presence_of(:email)
    end
  end

  describe 'age' do
    subject { User.create(name: "Matias", email: "matias@gmail.com", age: age) }

    context 'when user is younger than 18' do
      let(:age) { 16 }

      it 'is not valid' do
        expect(subject).to_not be_valid
      end
    end

    context 'and is older than 18' do
      let(:age) { 21 }

      it 'is valid' do
        expect(subject).to be_valid
      end
    end

    describe 'fully_adult' do
      context 'when the user is younger than 21' do
        let(:age) { 20 }

        it { is_expected.not_to be_fully_adult }
      end

      context 'when the user is older than 21' do
        let(:age) { 22 }

        it { is_expected.to be_fully_adult }
      end
    end

    describe '.fully_adults' do
      let!(:adult_matias) { User.create!(name: "Matias", email: "matias@gmail.com", age: 22) }
      let!(:adult_marcus) { User.create!(name: "Marcus", email: "marcus@gmail.com", age: 43) }
      let!(:teen_jules) { User.create!(name: "Jules", email: "jules@gmail.com", age: 18) }

      subject { User.fully_adults }

      it { is_expected.to contain_exactly(adult_matias, adult_marcus) }
    end

    describe 'bets' do
      let!(:matias) { User.create!(name: "Matias", email: "matias@gmail.com", age: 22) }
      let!(:poker_bet) { Bet.create!(amount: 199, description: "poker", user: matias) }
      let!(:horses_bet) { Bet.create!(amount: 199, description: "horses", user: matias) }
      let(:matias_bets) { [poker_bet, horses_bet] }

      it 'a user knows its bets' do
        expect(matias.bets).to match_array matias_bets
      end
    end
  end














  # describe 'valid' do
  #   let(:user_properties) { {} }
  #
  #   subject { User.create(user_properties).valid? }
  #
  #   context "when the user hasn't got a name" do
  #     it 'it is not valid' do
  #       expect(subject).to be_falsey
  #     end
  #   end
  #
  #   context "when the user has got a name" do
  #     let(:user_properties) { {name: "Matias"} }
  #
  #     context "but does not have an email" do
  #       it 'is not valid' do
  #         expect(subject).to be_falsey
  #       end
  #     end
  #
  #     context 'and does have an email' do
  #       let(:user_properties) { {name: "Matias", email: "matias@gmail.com"} }
  #
  #       it 'it is valid' do
  #         expect(subject).to be_truthy
  #       end
  #     end
  #   end
  # end
end
