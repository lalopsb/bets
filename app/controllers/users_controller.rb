class UsersController < ApplicationController

  skip_before_filter :verify_authenticity_token

  def index
    render json: params[:fully_adults] ? User.fully_adults : User.all , status: :ok
  end

  def create
    User.create!(user_params)
    render json: {}, status: :created
  end

  private

  def user_params
    params.permit(:name, :email, :age)
  end

end
