FactoryBot.define do
  factory :user do
    name Faker::Name.name
    email Faker::Internet.email
    age Random.rand(22..70)
  end

  trait :teen do
    age 18
  end

  trait :adult do
    age Random.rand(21..99)
  end
end