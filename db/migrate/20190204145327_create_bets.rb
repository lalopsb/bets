class CreateBets < ActiveRecord::Migration
  def change
    create_table :bets do |t|
      t.references :user
      t.integer :amount
      t.string :description
      t.timestamps null: false
    end
  end
end
